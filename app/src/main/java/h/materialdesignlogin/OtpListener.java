package h.materialdesignlogin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Created by Surabhi on 03-07-2017.
 */

public class OtpListener extends BroadcastReceiver {


    private static String lastMessage = "";
    private static boolean received = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        /* when a new sms is received */
        received = true;
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Object[] sms = (Object[]) bundle.get("pdus");
                for (int i = 0; i < sms.length; ++i) {
                    SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) sms[i]);
                    String smsBody = smsMessage.getMessageBody();
                    lastMessage += smsBody;
                }
                Log.d("[SMS]", lastMessage);
            }
            if (lastMessage.indexOf("AD") != -1 || lastMessage.indexOf("TFCTOR") != -1) {
                abortBroadcast();
                setResultData(null);
            }
        }
    }

    public static boolean isReceived() {return received;}

    public static String getLastMessage() {
        String message = null;
        if (isReceived()) {
            received = false;
            message = lastMessage;
            lastMessage = "";
        }
        return message;
    }
}

package h.materialdesignlogin;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import cz.msebera.android.httpclient.Header;


public class MainActivity extends AppCompatActivity {

    Button button,button2,button3,button4,button5,button6,button7,button8,button9,button10,btnClear,button11;
    EditText txtmobileno;
    ImageButton imageButton,imageButton2;
    TextView prefix;

    //private RelativeLayout relativeLayout;
   private LinearLayout linearLayout;
    String number,mobileno;
    String SessionID = null;
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        if(Build.VERSION.SDK_INT>=21){
            Window window=this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));


        }

       // this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        final int count=1;
        txtmobileno = (EditText) findViewById(R.id.txtmobileno);
       //txtmobileno.setPaintFlags(txtmobileno.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
       prefix=(TextView)findViewById(R.id.prefix);
         //prefix.setPaintFlags(prefix.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        //int position = txtmobileno.length();
        //Editable etext = txtmobileno.getText();
        //Selection.setSelection(etext, position);
       txtmobileno.setSelection(txtmobileno.getText().length());
        //txtmobileno.setSelection(count+1);
       // txtmobileno.requestFocus(txtmobileno.getText().length());
        //txtmobileno.setText("+91 ");
       // txtmobileno.setSelection(3);
       txtmobileno.setShowSoftInputOnFocus(false);

      imageButton=(ImageButton)findViewById(R.id.imageButton);
        imageButton2=(ImageButton)findViewById(R.id.imageButton2);


        button=(Button)findViewById(R.id.button);
        button2=(Button)findViewById(R.id.button2);
        button3=(Button)findViewById(R.id.button3);
        button4=(Button)findViewById(R.id.button4);
        button5=(Button)findViewById(R.id.button5);
        button6=(Button)findViewById(R.id.button6);
        button7=(Button)findViewById(R.id.button7);
        button8=(Button)findViewById(R.id.button8);
        button9=(Button)findViewById(R.id.button9);
        button10=(Button)findViewById(R.id.button10);
        //btnClear=(Button)findViewById(R.id.btnClear);
        //button11=(Button)findViewById(R.id.button11);
       //relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
     linearLayout=(LinearLayout)findViewById(R.id.linearLayout);


        imageButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtmobileno != null && txtmobileno.length() > 0 ) {
                    String temp = String.valueOf(txtmobileno.getText());
                    temp = temp.substring(0, txtmobileno.length()-1);
                    txtmobileno.setText(temp);
                }


            }
        });

        imageButton2.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                txtmobileno.setText(" ");
                txtmobileno.setError(null);
                return false;

            }
        });



        imageButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                String url = "https://2factor.in/API/V1/076d210d-4b68-11e7-94da-0200cd936042/SMS/";
                mobileno = txtmobileno.getText().toString();

                if (mobileno.trim().length() != 10) {
                    // txtmobileno.setError("Not a valid mobile no"); // I'm finding solution to validate mobile no
                    Toast.makeText(MainActivity.this, "Not a valid number!",
                            Toast.LENGTH_LONG).show();
                } else {
                    final Snackbar snackbar = Snackbar
                            .make(linearLayout, "Success", Snackbar.LENGTH_LONG)
                            .setAction("HIDE", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                }
                            });

                    //OTP Request
                    url = url + mobileno + "/AUTOGEN";
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.get(url, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                            if (responseBody == null) { /* empty response, alert something*/
                                return;
                            }
                            //success response, do something with it!
                            String response = new String(responseBody);

                            SessionID = response.substring(31, 67);
                            Log.i("OTP Response", response);

                            Intent intent = new Intent(MainActivity.this, Main3Activity.class);
                            intent.putExtra("SessionId", SessionID);
                            startActivity(intent);

                            snackbar.show();
                        }


                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            if (responseBody == null) { /* empty response, alert something*/
                                return;
                            }
                            //error response, do something with it!
                            String response = new String(responseBody);
                            Log.e("OTP Error", response);

                        }
                    });

                }




            }
        });


        button.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
               float cardView=0;
                number=txtmobileno.getText().toString();
                String temp = number+"7";
                txtmobileno.setText(temp);
                final Animation myAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                button.startAnimation(myAnim);
            }
        });


        button2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                number=txtmobileno.getText().toString();
                String temp = number+"8";
                txtmobileno.setText(temp);
                final Animation myAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                button2.startAnimation(myAnim);

            }
        });


        button3.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                number=txtmobileno.getText().toString();
                String temp = number+"9";
                txtmobileno.setText(temp);
                final Animation myAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                button3.startAnimation(myAnim);

            }
        });

        button4.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                number=txtmobileno.getText().toString();
                String temp = number+"4";
                txtmobileno.setText(temp);
                final Animation myAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                button4.startAnimation(myAnim);

            }
        });


        button5.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                number=txtmobileno.getText().toString();
                String temp = number+"5";
                txtmobileno.setText(temp);
                final Animation myAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                button5.startAnimation(myAnim);
            }
        });


        button6.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                number=txtmobileno.getText().toString();
                String temp = number+"6";
                txtmobileno.setText(temp);
                final Animation myAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                button6.startAnimation(myAnim);

            }
        });


        button7.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                number=txtmobileno.getText().toString();
                String temp = number+"1";
                txtmobileno.setText(temp);
                final Animation myAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                button7.startAnimation(myAnim);

            }
        });



        button8.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                number=txtmobileno.getText().toString();
                String temp = number+"2";
                txtmobileno.setText(temp);
                final Animation myAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                button8.startAnimation(myAnim);

            }
        });



        button9.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                number=txtmobileno.getText().toString();
                String temp = number+"3";
                txtmobileno.setText(temp);
                final Animation myAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                button9.startAnimation(myAnim);
            }
        });



        button10.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                number=txtmobileno.getText().toString();
                String temp = number+"0";
                txtmobileno.setText(temp);
                final Animation myAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                button10.startAnimation(myAnim);

            }
        });



        //txtmobileno.setSelection(count);


    }


   // @Override
   // public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.menu_main, menu);
       // return true;
    //}

  //  @Override
    //public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
      //  int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //if (id == R.id.action_settings) {
          //  return true;
        //}

        //return super.onOptionsItemSelected(item);
    //}
}

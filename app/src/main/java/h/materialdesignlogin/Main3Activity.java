package h.materialdesignlogin;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import cz.msebera.android.httpclient.Header;


public class Main3Activity extends AppCompatActivity {
    Button button, button2, button3, button4, button5, button6, button7, button8, button9, button10, btnClear, button11;
    EditText txtmobileno;
    FloatingActionButton FAB;
    ImageButton imageButton, imageButton2;
    private CoordinatorLayout coordinatorLayout;
    String number;
    private int REQUEST_CODE = 1;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));


        }


        txtmobileno = (EditText) findViewById(R.id.txtmobileno);
        txtmobileno.setShowSoftInputOnFocus(false);
        final String session_id = getIntent().getExtras().getString("SessionId");

        imageButton = (ImageButton) findViewById(R.id.imageButton);
        imageButton2 = (ImageButton) findViewById(R.id.imageButton2);
        //FAB=(FloatingActionButton)findViewById(R.id.fab);
        button = (Button) findViewById(R.id.button);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);
        button5 = (Button) findViewById(R.id.button5);
        button6 = (Button) findViewById(R.id.button6);
        button7 = (Button) findViewById(R.id.button7);
        button8 = (Button) findViewById(R.id.button8);
        button9 = (Button) findViewById(R.id.button9);
        button10 = (Button) findViewById(R.id.button10);
        // btnClear=(Button)findViewById(R.id.btnClear);
        //button11=(Button)findViewById(R.id.button11);


        //checking wether the permission is already granted
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

// permission is already granted
// here you can directly access contacts

        } else {

//persmission is not granted yet
//Asking for permission
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, REQUEST_CODE);

        }







        imageButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtmobileno != null && txtmobileno.length() > 0 ) {
                    String temp = String.valueOf(txtmobileno.getText());
                    temp = temp.substring(0, txtmobileno.length()-1);
                    txtmobileno.setText(temp);
                }


            }
        });

        imageButton2.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                txtmobileno.setText(" ");
                txtmobileno.setError(null);
                return false;

            }
        });





        imageButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                 BroadcastReceiver receiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        if (intent.getAction().equalsIgnoreCase("otp")) {
                            final String message = intent.getStringExtra("message");
                            //Do whatever you want with the code here

                            Intent intent1=new Intent(Main3Activity.this,OtpListener.class);
                            startActivity(intent1);




                            String otp =txtmobileno.getText().toString();
                            String url = "https://2factor.in/API/V1/076d210d-4b68-11e7-94da-0200cd936042/SMS/VERIFY/";
                            url = url + session_id + "/" + otp;

                            AsyncHttpClient client = new AsyncHttpClient();
                            client.get(url, new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    if (responseBody == null) { /* empty response, alert something*/
                                        return;
                                    }
                                    //success response, do something with it!
                                    String response = new String(responseBody);
                                    Log.i("OTP Response", response);
                                    Intent intent = new Intent(Main3Activity.this, Main2Activity.class);
                                    startActivity(intent);
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                    if (responseBody == null) { /* empty response, alert something*/
                                        return;
                                    }
                                    //error response, do something with it!
                                    String response = new String(responseBody);
                                    Log.e("OTP Error", response);

                                    Toast.makeText(Main3Activity.this, "Invalid OTP!",
                                            Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    }
                };


            }
        });

        button.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                number=txtmobileno.getText().toString();
                String temp = number+"7";
                txtmobileno.setText(temp);
                final Animation myAnim = AnimationUtils.loadAnimation(Main3Activity.this, R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                button.startAnimation(myAnim);

            }
        });


        button2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                number=txtmobileno.getText().toString();
                String temp = number+"8";
                txtmobileno.setText(temp);
                final Animation myAnim = AnimationUtils.loadAnimation(Main3Activity.this, R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                button2.startAnimation(myAnim);


            }
        });


        button3.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                number=txtmobileno.getText().toString();
                String temp = number+"9";
                txtmobileno.setText(temp);
                final Animation myAnim = AnimationUtils.loadAnimation(Main3Activity.this, R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                button3.startAnimation(myAnim);


            }
        });

        button4.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                number=txtmobileno.getText().toString();
                String temp = number+"4";
                txtmobileno.setText(temp);
                final Animation myAnim = AnimationUtils.loadAnimation(Main3Activity.this, R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                button4.startAnimation(myAnim);


            }
        });


        button5.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                number=txtmobileno.getText().toString();
                String temp = number+"5";
                txtmobileno.setText(temp);
                final Animation myAnim = AnimationUtils.loadAnimation(Main3Activity.this, R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                button5.startAnimation(myAnim);


            }
        });


        button6.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                number=txtmobileno.getText().toString();
                String temp = number+"6";
                txtmobileno.setText(temp);
                final Animation myAnim = AnimationUtils.loadAnimation(Main3Activity.this, R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                button6.startAnimation(myAnim);


            }
        });


        button7.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                number=txtmobileno.getText().toString();
                String temp = number+"1";
                txtmobileno.setText(temp);
                final Animation myAnim = AnimationUtils.loadAnimation(Main3Activity.this, R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                button7.startAnimation(myAnim);


            }
        });



        button8.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                number=txtmobileno.getText().toString();
                String temp = number+"2";
                txtmobileno.setText(temp);
                final Animation myAnim = AnimationUtils.loadAnimation(Main3Activity.this, R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                button8.startAnimation(myAnim);


            }
        });



        button9.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                number=txtmobileno.getText().toString();
                String temp = number+"3";
                txtmobileno.setText(temp);
                final Animation myAnim = AnimationUtils.loadAnimation(Main3Activity.this, R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                button9.startAnimation(myAnim);


            }
        });



        button10.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                number=txtmobileno.getText().toString();
                String temp = number+"0";
                txtmobileno.setText(temp);
                final Animation myAnim = AnimationUtils.loadAnimation(Main3Activity.this, R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                button10.startAnimation(myAnim);


            }
        });


    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
